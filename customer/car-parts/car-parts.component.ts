import { Component } from '@angular/core';
import { ConfigService } from '../../core/services/config.service';

@Component({
    selector: 'car-parts',
    templateUrl: './car-parts.component.html'
})
export class CarPartsComponent {
    public showAllOrders: boolean = true;
    public tr5Url: string = '';

    constructor(private config: ConfigService) {
        this.tr5Url = this.config.get('tr5Url');
    }

    public toggleOrders(): void {
        this.showAllOrders = !this.showAllOrders;
    }
}
