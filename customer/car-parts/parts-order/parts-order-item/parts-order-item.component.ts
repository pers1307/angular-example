/**
 * Created by asmolin on 27.06.17.
 */
import { Component, Input } from '@angular/core';
import { CustomerOrderParts } from '../../../../core/models/customer-order-parts';

@Component({
    selector       : 'parts-order-item',
    templateUrl    : './parts-order-item.html'
})
export class PartsOrderItem {
    @Input() order: CustomerOrderParts;

    constructor() {
    }

}
