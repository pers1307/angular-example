/**
 * Created by asmolin on 26.05.17.
 */
import { Component, Input, OnInit } from '@angular/core';
import { CustomerOrderParts } from '../../../core/models/customer-order-parts';
import { CustomerCar } from '../../../core/models/customer-car';
import { PartsOrderService } from '../../../core/services/parts-order.service';
import { CustomerCarService } from '../../../core/services/customer-car.service';
import { ConfigService } from '../../../core/services/config.service';

@Component({
    selector       : 'parts-order',
    templateUrl    : './parts-order.html'
})
export class PartsOrder implements OnInit {
    @Input()
    public showClosedOrders: boolean = true;

    public closedOrders: Array<CustomerOrderParts>;
    public openOrders: Array<CustomerOrderParts>;
    public car: CustomerCar;
    public tr5Url: string;
    public orderParts: Array<CustomerOrderParts> = [];
    public load: boolean = false;

    constructor(
        private partsOrderService: PartsOrderService,
        private customerCarService: CustomerCarService,
        private config: ConfigService) {

        this.tr5Url = this.config.get('tr5Url');
    }

    ngOnInit(): void {
        this.customerCarService.customerCarCurrent$
            .subscribe(customerCar => {
                this.car = customerCar;

                if (customerCar) {
                    this.partsOrderService.getPartOrdersAll({
                        order: 'desc',
                    }).subscribe(data => {
                        this.orderParts = data;
                        this.load = true;

                        this.closedOrders = this.orderParts.filter(order => order.close);
                        this.openOrders = this.orderParts.filter(order => !order.close);
                    });
                }
            });
    }
}
