import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarPartsComponent } from './car-parts.component';
import { PartsOrder } from './parts-order/parts-order.component';
import { PartsOrderItem } from './parts-order/parts-order-item/parts-order-item.component';
import { RouterModule } from '@angular/router';
import { PartsOrderForCarComponent } from './parts-order-for-car/parts-order-for-car.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [
        CarPartsComponent,
        PartsOrder,
        PartsOrderItem,
        PartsOrderForCarComponent
    ],
    exports: [
        PartsOrder,
        PartsOrderItem,
        PartsOrderForCarComponent
    ]
})
export class CarPartsModule {

}
