import { Component, OnInit } from '@angular/core';
import { PartsOrderService } from '../../../core/services/parts-order.service';
import { CustomerCarService } from '../../../core/services/customer-car.service';
import { CustomerOrderParts } from '../../../core/models/customer-order-parts';
import { ConfigService } from '../../../core/services/config.service';

@Component({
    selector: 'parts-order-for-car',
    templateUrl: './parts-order-for-car.component.html'
})
export class PartsOrderForCarComponent implements OnInit {
    public orders: Array<CustomerOrderParts> = [];
    public tr5Url: string;
    public load: boolean = false;

    constructor(private partsOrderService: PartsOrderService,
                private customerCarService: CustomerCarService,
                private config: ConfigService) {

        this.tr5Url = this.config.get('tr5Url');
    }

    public ngOnInit(): void {
        this.customerCarService.customerCarCurrent$
            .filter(customerCurrentCar => customerCurrentCar !== null)
            .flatMap(customerCar => {
                return this.partsOrderService.getPartOrdersAll({
                    order: 'desc',
                    close: false
                }).map((orders) => {
                    return orders.filter(order => order.customerCarId === customerCar.id);
                });
            }).subscribe(orders => {
                this.orders = orders;
                this.load = true;
        });
    }
}
