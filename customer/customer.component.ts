import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { CustomerCarService } from '../core/services/customer-car.service';
import { CompanyService } from '../core/services/company.service';
import { UpdateDataService } from '../core/services/update-data.service';
import { ConfigService } from '../core/services/config.service';
import { CustomerCar } from '../core/models/customer-car';
import { AuthService } from '../core/services/auth.service';
import { Chat2deskService } from '../shared/services';

@Component({
    selector: 'cabinet',
    templateUrl: './customer.component.html'
})
export class Customer implements OnInit, OnDestroy {

    public companiesSubscription: Subscription;
    public fullLoaded: boolean = false;

    private user;
    private cars: Array<CustomerCar>;

    constructor(
        private auth: AuthService,
        private customerCarService: CustomerCarService,
        private companyService: CompanyService,
        private updateDataService: UpdateDataService,
        private config: ConfigService,
        private chat2desk: Chat2deskService
    ) {
    }

    public ngOnInit(): void {
        this.customerCarService.customerCarList$.subscribe(cars => this.cars = cars);
        this.auth.user$.subscribe(user => {
            this.user = user
        });

        // Если есть модуль по тачкам (dashboard) то получаем инфу о тачках
        if ('dashboard' in this.config.get('menu')) {
            this.customerCarService.customerCarCurrent$
                .subscribe(currentCar => {
                    if (currentCar) {
                        this.companiesSubscription = this.companyService.getList(true, currentCar).subscribe();
                    }
                });

            if (this.updateDataService.updateStart === true) {
                this.updateDataService.retrievingStatus$
                    .mergeMap(status => {
                        return this.customerCarService.customerCarCurrent$;
                    }).subscribe((currentCustomerCar) => {
                        this.fullLoaded = true;
                    });
            } else {
                this.fullLoaded = true;
            }
        } else {
            this.fullLoaded = true;
        }
    }

    public ngOnDestroy(): void {
        if (this.companiesSubscription) {
            this.companiesSubscription.unsubscribe();
        }
    }
}
