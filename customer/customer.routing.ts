import { Routes, RouterModule } from '@angular/router';
import { Customer } from './customer.component';
import { ModuleWithProviders } from '@angular/core';
import { CustomerGuard } from './customer.guard';
import { ConfigService } from '../core/services/config.service';

import { ArrayUtils } from '../shared/utils/array.utils';
import { AvailableModulesService } from '../core/services/available-modules.service';
import { DashboardLayoutGuard } from './dashboard-layout/dashboard-layout.guard';
import { CustomerScenarioGuard } from './customer-scenario.guard';

const menuTree = ConfigService.get('menu');
const availableModules: Array<string> = AvailableModulesService.getAvailableModules(menuTree);
const defaultModule = ArrayUtils.inArray(availableModules, 'dashboard') ? 'dashboard' : 'request-history';

let childrenRoutes: Routes = [
    {path: '', redirectTo: defaultModule, pathMatch: 'full'}
];

if (ArrayUtils.inArray(availableModules, 'dashboard')) {
    childrenRoutes = childrenRoutes.concat([
        {
            path: 'dashboard',
            loadChildren: 'app/customer/dashboard-layout/dashboard-layout.module#DashboardLayoutModule',
            canActivate: [DashboardLayoutGuard]
        }
    ]);
}

if (ArrayUtils.inArray(availableModules, 'request-history')) {
    childrenRoutes = childrenRoutes.concat([
        {
            path: 'request-history',
            loadChildren: 'app/customer/request-history-layout/request-history-layout.module#RequestHistoryLayoutModule',
        }
    ]);
}

export let routes: Routes = [
    {
        path       : '',
        component  : Customer,
        children   : childrenRoutes,
        canActivate: [CustomerGuard, CustomerScenarioGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
