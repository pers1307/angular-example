import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './request-history-layout.routing';
import { RequestHistoryLayoutComponent } from './request-history-layout.component';
import { NgaModule } from '../../shared/nga.module';
import { CarPartsModule } from '../car-parts/car-parts.module';

@NgModule({
    imports: [
        routing,
        CommonModule,
        NgaModule,
        CarPartsModule,
    ],
    declarations: [
        RequestHistoryLayoutComponent,
    ]
})
export class RequestHistoryLayoutModule {

}
