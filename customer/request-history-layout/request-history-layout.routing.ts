import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ArrayUtils } from '../../shared/utils/array.utils';

import { AvailableModulesService } from '../../core/services/available-modules.service';
import { ConfigService } from '../../core/services/config.service';
import { RequestHistoryLayoutComponent } from './request-history-layout.component';
import { CustomerGuard } from '../customer.guard';
import { CarPartsComponent } from '../car-parts/car-parts.component';

const menuTree = ConfigService.get('menu');
const availableModules: Array<string> = AvailableModulesService.getAvailableModules(menuTree);

let children: Routes = [
    {
        path: '',
        redirectTo: 'orders',
        pathMatch: 'full'
    },
    {
        path: 'orders',
        loadChildren: 'app/customer/request-history-layout/orders/orders.module#OrdersModule'
    }
];

if (ArrayUtils.inArray(availableModules, 'car-parts')) {
    children = children.concat([
        {
            path: 'car-parts',
            component: CarPartsComponent
        }
    ]);
}

export let routes: Routes = [
    {
        path: '',
        component: RequestHistoryLayoutComponent,
        children: children,
        canActivate: [CustomerGuard]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
