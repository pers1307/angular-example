import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './orders.routing';

import { Orders } from './orders.component';
import { OrderInfo } from './order-info/order-info.component';

@NgModule({
    imports: [
        CommonModule,
        routing,
    ],
    declarations: [
        Orders,
        OrderInfo
    ]
})
export class OrdersModule {
}
