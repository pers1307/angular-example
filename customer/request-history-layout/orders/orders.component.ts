import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../core/services/order.service';
import { Order } from '../../../core/models';
import { PaymentOrderService } from '../../../core/services/payment.order.service';
import { ConfigService } from '../../../core/services/config.service';

@Component({
    selector: 'customer-orders',
    templateUrl: './orders.html'
})

export class Orders implements OnInit {

    public orders: Array<Order>;
    public load = false;
    public currentCanceledOrder: Order;

    public theme: string = process.env.THEME;
    public newCarsTr5Url: string = this.config.get('newCarsTr5Url');
    public usedCarsTr5Url: string = this.config.get('usedCarsTr5Url');
    public isPaymentEnabled = this.config.get('isPaymentEnabled');

    private ORDER_STATUS_RESERVED: number = 2;
    private ORDER_STATUS_CANCELED: number = 4;
    private alert: string;

    constructor(private orderService: OrderService,
                private paymentOrderService: PaymentOrderService,
                private config: ConfigService) {
    }

    public ngOnInit(): void {
        this.load = true;

        this.orderService.getList().subscribe(data => {
            this.orders = data;
            this.load = false;
        });
    }

    public isStatusReserved(order: Order): boolean {
        return order.status === this.ORDER_STATUS_RESERVED;
    }

    public cancelOrder(order: Order): void {

        if (!confirm('Вы уверены, что хотите отменить заказ?')) {
            return;
        }

        this.load = true;

        this.currentCanceledOrder = order;
        this.paymentOrderService.cancel(order.id).subscribe(data => {
            if (data.success && !data.errors) {
                order.status = this.ORDER_STATUS_CANCELED;
                order.statusTitle = data.status;
                this.alert = 'Заказ отменен';
            } else {
                this.alert = data.errors;
            }

            this.load = false;
        });
    }

    public closeAlert(): void {
        this.alert = null;
    }
}
