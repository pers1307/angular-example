import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ConfigService } from '../../../core/services/config.service';

import { Orders } from './orders.component';
import { OrderInfo } from './order-info/order-info.component';



let routes: Routes = [
    {
        path: '',
        component: Orders
    }
];

if (ConfigService.get('isPaymentEnabled')) {
    routes = routes.concat([
        {
            path: ':id',
            component: OrderInfo
        }
    ]);
}

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
