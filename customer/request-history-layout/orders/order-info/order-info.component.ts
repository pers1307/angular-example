import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../../core/services/order.service';
import { ActivatedRoute } from '@angular/router';
import { Order } from '../../../../core/models';
import { PaymentOrderService } from '../../../../core/services/payment.order.service';

@Component({
    selector: 'customer-order-info',
    templateUrl: './order-info.html'
})
export class OrderInfo implements OnInit {

    public order: Order;
    public load = false;
    private orderId: number;
    private alert: string;

    private ORDER_STATUS_RESERVED:number = 2;
    private ORDER_STATUS_CANCELED:number = 4;

    constructor(private orderService: OrderService,
                private activateRoute: ActivatedRoute,
                private paymentOrderService: PaymentOrderService
    ) {
        this.orderId = activateRoute.snapshot.params['id'];
    }

    public ngOnInit():void {
        this.load = true;

        this.orderService.findById(this.orderId).subscribe(data => {
            this.order = data;
            this.load = false;
        });
    }

    public isStatusReserved():boolean {
        return this.order.status == this.ORDER_STATUS_RESERVED;
    }

    public cancelOrder():void {

        if (!confirm('Вы уверены, что хотите отменить заказ?')) {
            return;
        }

        this.load = true;

        this.paymentOrderService.cancel(this.orderId).subscribe(data => {
            if (data.success && !data.errors) {
                this.order.status = this.ORDER_STATUS_CANCELED;
                this.order.statusTitle = data.status;
                this.alert = 'Заказ отменен';
            } else {
                this.alert = data.errors;
            }

            this.load = false;
        });
    }

    public closeAlert():void {
        this.alert = null;
    }

}