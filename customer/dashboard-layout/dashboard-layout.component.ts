import { Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { CompressStateService } from './services/compress-state.service';
import { ServiceCampaignAlertService } from '../../core/services/service-campaign-alert.service';
import { WindowResizeService } from '../../shared/services';
import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

@Component({
    selector: 'dashboard-layout',
    templateUrl: './dashboard-layout.component.html',
    animations: [
        trigger('pageAnimation', [
            state('in', style({transform: 'translateX(0)'})),
            transition('void => *', [
              style({transform: 'translateX(100%)'}),
              animate(300)
            ]),
            transition('* => void', [
              animate(300, style({transform: 'translateX(100%)'}))
            ])
        ]),
        trigger('sidebarAnimation', [
            state('in', style({transform: 'translateX(0)'})),
            transition('* => void', [
              animate(0, style({transform: 'translateX(-100%)'}))
            ]),
            transition('void => *', [
              style({transform: 'translateX(-100%)'}),
              animate(300)
            ]),
        ])
      ]
})
export class DashboardLayoutComponent implements OnInit, OnDestroy {
    public isPanelsCompressed: boolean = false;
    public compressStateSubscription: Subscription;
    public windowResizeSubscription: Subscription;
    public hasServiceCampaigns: boolean;
    public isEntered: boolean = false;
    public isMobile: boolean = false;
    public isAnimating:boolean  = false;

    constructor(
        private compressStateService: CompressStateService,
        private serviceCampaignAlertService: ServiceCampaignAlertService,
        private windowResizeService: WindowResizeService,
        private cd: ChangeDetectorRef
    ) {
    }

    public ngOnInit(): void {
        this.compressStateSubscription = this.compressStateService.getState().subscribe(state => this.isPanelsCompressed = state);
        this.serviceCampaignAlertService.isAlertShow$.asObservable().debounceTime(10).subscribe(state => this.hasServiceCampaigns = state);
        this.windowResizeSubscription = this.windowResizeService.getState().subscribe(state => {
            this.isMobile = state;
            this.cd.detectChanges();
        });
    }

    public ngOnDestroy(): void {
        this.compressStateSubscription.unsubscribe();
        this.windowResizeSubscription.unsubscribe();
    }

    public back(): void {
        this.isEntered = false;
    };

    public isEnteredOnMobile() {
        if (!this.isMobile) {
            return true;
        } else {
            return this.isEntered;
        }
    }

    onEnter(): void {
        if (this.isMobile) {
            this.isEntered = true;
        }
    }

    animationStarted(): void {
        this.isAnimating = true;
    }

    animationDone(): void {
        this.isAnimating = false;
    }
}
