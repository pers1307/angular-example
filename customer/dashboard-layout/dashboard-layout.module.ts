import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './dashboard-layout.routing';
import { NgaModule } from '../../shared/nga.module';

import { CompressDirective } from './directives/compress.directive';
import { CompressStateService } from './services/compress-state.service';
import { DashboardLayoutComponent } from './dashboard-layout.component';
import { Dashboard } from './dashboard/dashboard.component';
import { ServiceComponent } from './service/service.component';
import { ServiceHistoryComponent } from './service/service-history/service-history.component';
import { ServiceHistoryItemComponent } from './service/service-history/service-history-item/service-history-item.component';
import { InsuranceDashboard } from './insurance-dashboard/insurance-dashboard.component';
import { InsuranceOrder } from './insurance-dashboard/insurance-order/insurance-order.component';
import { InsuranceOrderItem } from './insurance-dashboard/insurance-order/insurance-order-item/insurance-order-item.component';
import { SellCarComponent } from './sell-car/sell-car.component';
import { CarPartsModule } from '../car-parts/car-parts.module';
import { ServiceCampaignItem } from './service-campaign/service-campaign-item/service-campaign-item.component';
import { ServiceCampaign } from './service-campaign/service-campaign.component';
import { ServiceCampaignService } from './services/service-campaign.service';

@NgModule({
    imports: [
        CommonModule,
        NgaModule,
        routing,
        CarPartsModule
    ],
    declarations: [
        CompressDirective,
        DashboardLayoutComponent,
        Dashboard,
        ServiceComponent,
        ServiceHistoryComponent,
        ServiceHistoryItemComponent,
        InsuranceDashboard,
        InsuranceOrder,
        InsuranceOrderItem,
        SellCarComponent,
        ServiceCampaign,
        ServiceCampaignItem
    ],
    providers: [
        CompressStateService
    ]
})
export class DashboardLayoutModule {
}
