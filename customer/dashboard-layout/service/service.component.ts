import { Component, OnInit, OnDestroy } from '@angular/core';
import { CustomerOrderService, CustomerCarService } from '../../../core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerOrder, CustomerCar, CustomerFutureOrder } from '../../../core/models';
import { Subscription } from 'rxjs/Subscription';
import { TechnicalService as TechService } from '../../../core/services/technical-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleForm } from '../../../shared/components/simple-form/simple-form.component';
import { Anchor } from '../../../core/models/anchor';

import 'style-loader!./service.scss';
import 'style-loader!app/shared/sass/blocks/_service-history.scss';
import 'style-loader!app/shared/sass/blocks/_tech-order.scss';
import 'style-loader!app/shared/sass/blocks/_pricing-list.scss';
import { ScrollToAnchorService } from '../../../shared/services/scroll-to-anchor/scroll-to-anchor.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Component({
    selector   : 'service',
    templateUrl: './service.component.html'
})
export class ServiceComponent implements OnInit, OnDestroy {

    public ordersForCarCompleteEvent$: Subject<boolean> = new Subject();
    public changeCarCompleteEvent$: Subject<boolean> = new Subject();

    public orders: Array<CustomerOrder> = [];
    public futureOrders: Array<CustomerFutureOrder> = [];
    public selectedOrder: CustomerOrder;
    public customerCar: CustomerCar = null;
    public load: boolean;
    public anchors: Array<Anchor>;

    private changeCarSubscription: Subscription;
    private getCarSubscription:    Subscription;

    constructor(private customerOrderService: CustomerOrderService,
                private customerCarService: CustomerCarService,
                private route: ActivatedRoute,
                private router: Router,
                private technicalService: TechService,
                private modalService: NgbModal,
                private _scrollToAnchorService: ScrollToAnchorService
    ) {
        this.anchors = [
            {
                anchor: 'current-service',
                description: 'Текущее обслуживание',
                icon: 'clock'
            },
            /*{
                anchor: 'profit',
                description: 'Выгодные предложения',
                icon: 'percent_thin'
            },*/
            {
                anchor: 'service-history',
                description: 'История обслуживания',
                icon: 'list-with-checks',
            }
        ];
        this.registerEvent();
    }

    onSelect(order: CustomerOrder): void {
        if (!order) {
            order = this.orders[0];
        }

        this.selectedOrder = order;
    };

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            if (params.id !== undefined) {
                this.redirectDeleteId(params);
            } else {
                this.initChangeCarSubscription(params);
            }
        });


    }

    ngOnDestroy(): void {
        if (this.changeCarSubscription !== undefined) {
            this.changeCarSubscription.unsubscribe();
        }

        if (this.getCarSubscription !== undefined) {
            this.getCarSubscription.unsubscribe();
        }
    }

    public openForm(event: any, car: CustomerCar) {
        const modalRef = this.modalService.open(SimpleForm);
        modalRef.componentInstance.customerCar = car;

        let comment = 'Запись на сервисное обслуживание по рекомендации:';
        this.selectedOrder.recommendations.forEach(function (recomendation) {
            comment += ' ' + recomendation['description'] + ';';
        });

        modalRef.componentInstance.comment = comment;
        modalRef.componentInstance.type = 'technical_service';
    }

    private redirectDeleteId(params): void {
        this.getCarSubscription = this.customerCarService.getCar(Number(params.id))
            .subscribe(customerCar => {
                if (customerCar) {
                    this.customerCarService.selectCar(customerCar);

                    if (params.orderId !== undefined) {
                        this.router.navigate(['service/order', params.orderId], {replaceUrl : true});
                    } else {
                        this.router.navigate(['dashboard/service']);
                    }
                }
            });
    }

    private initChangeCarSubscription(params): void {
        this.changeCarSubscription = this.customerCarService.customerCarCurrent$
            .filter(selectedCar => selectedCar !== null)
            .subscribe(selectedCar => {
                if (
                    this.customerCar !== null
                    && this.customerCar.id !== selectedCar.id
                ) {
                    this.customerCar = selectedCar;
                    this.router.navigate(['dashboard/service']);
                }
                this.customerCar = selectedCar;

                this.getOrdersForCar(this.customerCar.id, params.orderId);

                this.changeCarCompleteEvent$.next(true);
                this.changeCarCompleteEvent$.complete();
            });
    }

    private getOrdersForCar(id: number, orderId: any = undefined) {
        this.load = true;

        this.customerOrderService.getOrdersForCar(this.customerCar.id)
            .subscribe(data => {
                this.orders = data;
                let order = null;

                if (this.orders.length) {
                    if (orderId !== undefined) {

                        if (orderId === 'opened') {
                            order = this.orders.find(function (_order: CustomerOrder) {
                                return !_order.closeDate;
                            });
                        } else if (orderId) {
                            order = this.orders.find(function (_order: CustomerOrder) {
                                return Number(orderId) === _order.id;
                            });
                        }
                    }
                }

                this.getAllFutureOrders();

                this.onSelect(order);
                this.load = false;
                this.ordersForCarCompleteEvent$.next(true);
                this.ordersForCarCompleteEvent$.complete();
            })
        ;
    }

    private getAllFutureOrders()
    {
        this.futureOrders = [];

        this.technicalService.getDateFutureTechnicalService(this.customerCar.id)
            .subscribe(data => {
                if (data !== 'noData') {
                    this.futureOrders.push({
                        date : new Date(data.date),
                        type : 'futureTechnicalService',
                        typeTitle: data.typeTitle
                    });
                }
            });
    }

    private registerEvent() {
        let subscription = Observable.forkJoin(this.ordersForCarCompleteEvent$, this.changeCarCompleteEvent$).subscribe(data => {
            if (data[0] && data[1]) {
                setTimeout(() => {
                    this._scrollToAnchorService.responsesComplete$.next(true);
                    this._scrollToAnchorService.responsesComplete$.complete();
                });

            }
        });
    }
}
