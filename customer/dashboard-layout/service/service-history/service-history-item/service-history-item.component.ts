import { Component, Input } from '@angular/core';
import { CustomerOrder } from '../../../../../core/models/customer-order';

@Component({
    selector: 'service-history-item',
    templateUrl: './service-history-item.component.html'
})
export class ServiceHistoryItemComponent {
    @Input() order: CustomerOrder;
    @Input() isActive: boolean;

    constructor() {

    }
}

