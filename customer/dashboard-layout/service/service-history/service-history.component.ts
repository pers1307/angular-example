import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CustomerOrder } from '../../../../core/models/customer-order';
import { Work } from '../../../../core/models/work';
import * as _ from 'lodash';

require('../../../../shared/icons/caret-in-circle_right.svg');
require('../../../../shared/icons/caret-in-circle_left.svg');

interface SeparatedOrderDates {
    year: string;
    months: Array<Date>;
}

@Component({
    selector: 'service-history',
    templateUrl: './service-history.component.html'
})
export class ServiceHistoryComponent implements OnChanges {
    @Input() orders: Array<CustomerOrder>;
    @Input() activeOrder: CustomerOrder;
    public SLIDE_WIDTH: number = 370;
    public selectedOrder: CustomerOrder;
    public sliderPosition: number = 0;
    public currentSlideIndex: number = 0;
    public slideStep: number = 1;
    public works: Array<Work>;
    public load: boolean = false;
    public filteredOrders: Array<CustomerOrder> = [];
    public separatedOrderDateCollection: Array<any> = [];

    constructor() {
    }

    selectOrderAndMove(order: CustomerOrder, index: number): void {
        this.selectOrder(order);

        /*
        * Если заказов более 3 - включаем слайдер
        */
        if (this.filteredOrders.length > 2) {
            if (index > this.currentSlideIndex) {
                this.next(index);
            } else {
                this.prev(index);
            }
        }
    };

    selectOrder(order: CustomerOrder): void {
        this.selectedOrder = order;
        this.getCompletedWorks();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.orders && !changes.orders.isFirstChange()) {

            this.orders = this.orders.filter(order => {
                return order.closed;
            });

            this.separatedOrderDateCollection = (_.chain(this.orders) as any)
                .reduce((acc, order) => {
                    let year = order.creationDate.getFullYear();

                    if (!(year in acc)) {
                        acc[year] = [];
                        acc[year].push(order.creationDate);
                    } else {
                        if (acc[year].map(date => date = date.getMonth()).indexOf(order.creationDate.getMonth()) === -1) {
                            acc[year].push(order.creationDate);
                        }
                    }

                    return acc;
                }, {})
                .map((value, key) => {
                    return {year: key, months: value.reverse()} as SeparatedOrderDates;
                }).value();

            this.separatedOrderDateCollection.reverse();
            this.load = true;
            this.selectedOrder = this.activeOrder;
            this.filteredOrders = this.orders;

            if (this.selectedOrder) {
                this.getCompletedWorks();
            }

            if (this.separatedOrderDateCollection.length) {
                this.filterOrdersByMonthsOfYear();
            }
        }
    }

    public getCompletedWorks() {
        this.works = this.selectedOrder.works.filter(work => !work.addWork);
    }

    public next(index) {
        if (index) {
            this.slideStep = Math.abs(index - this.currentSlideIndex);

            if (this.currentSlideIndex <= this.filteredOrders.length) {

                this.sliderPosition = this.sliderPosition - (this.SLIDE_WIDTH * this.slideStep);
                this.currentSlideIndex = index;
            }
        } else {
            if (this.currentSlideIndex <= this.filteredOrders.length - 2) {
                this.sliderPosition = this.sliderPosition - this.SLIDE_WIDTH;
                this.currentSlideIndex++;
                this.selectedOrder = this.filteredOrders[this.currentSlideIndex];
            }
        }
    }

    public prev(index) {
        if (index) {
            this.slideStep = Math.abs(index - this.currentSlideIndex);

            if (this.sliderPosition <= 0 && this.currentSlideIndex > 0) {

                this.sliderPosition = this.sliderPosition + (this.SLIDE_WIDTH * this.slideStep);
                this.currentSlideIndex = index;
            }
        } else {
            if (this.sliderPosition <= 0 && this.currentSlideIndex > 0) {
                this.sliderPosition = this.sliderPosition + this.SLIDE_WIDTH;
                this.currentSlideIndex--;
                this.selectedOrder = this.filteredOrders[this.currentSlideIndex];
            }
        }
    }

    public filterOrdersByYear(year?): void {
        year = Number(year) || Number(this.separatedOrderDateCollection[0].year);
        
        this.filteredOrders = this.orders.filter(order => {
            return order.creationDate.getFullYear() === year;
        });

        this.reset();
    }

    public filterOrdersByMonthsOfYear(year?, month?): void {
        year = Number(year) || Number(this.separatedOrderDateCollection[0].year);

        if (month) {
            month = month.getMonth();
        } else {
            month = this.separatedOrderDateCollection[0].months[0].getMonth();
        }

        this.filteredOrders = this.orders.filter(order => {
            return order.creationDate.getFullYear() === year && order.creationDate.getMonth() === month;
        });

        this.reset();
    }

    public reset(): void {
        this.sliderPosition = 0;
        this.currentSlideIndex = 0;
        this.selectOrder(this.filteredOrders[0]);
    }

    public changePeriod(state): void {
        if (!state) {
            this.filterOrdersByMonthsOfYear();
        } else {
            this.filterOrdersByYear();
        }
    }

    public isActiveDate(date, period): boolean {
        date = new Date(date);

        switch (period) {
            case 'month':
                return this.selectedOrder.creationDate.getMonth() === date.getMonth() && this.selectedOrder.creationDate.getFullYear() === date.getFullYear();
            case 'year':
                return this.selectedOrder.creationDate.getFullYear() === date.getFullYear();
            default:
                break;
        }
    }
}
