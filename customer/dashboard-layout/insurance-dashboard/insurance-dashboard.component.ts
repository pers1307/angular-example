/**
 * Created by aperevalov on 18.10.17.
 */
import { Component, OnInit } from '@angular/core';
import { CustomerCar } from '../../../core/models/customer-car';
import { CustomerCarService } from '../../../core/services/customer-car.service';

@Component({
    selector: 'insurance-dashboard',
    templateUrl: './insurance-dashboard.component.html'
})
export class InsuranceDashboard implements OnInit {
    public car: CustomerCar;

    constructor(private customerCarService: CustomerCarService) {

    }

    public ngOnInit(): void {
        this.customerCarService.customerCarCurrent$
            .subscribe(currentCar => {
                this.car = currentCar;
            });
    }
}
