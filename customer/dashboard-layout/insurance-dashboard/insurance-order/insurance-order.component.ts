import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CustomerCar } from '../../../../core/models/customer-car';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InsuranceForm } from '../../../../shared/components/insurance-form/insurance-form.component';
import { InsuranceService } from '../../../../core/services/insurance.service';

@Component({
    selector: 'insurance-order',
    templateUrl: './insurance-order.html'
})
export class InsuranceOrder implements OnChanges {
    @Input() car: CustomerCar;
    public orders = {};

    constructor(private modalService: NgbModal,
                private insuranceService: InsuranceService
    ) {
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (this.car) {
            this.orders = this.insuranceService.getInsuranceFromCustomerCar(this.car);
        }
    }

    public openForm() {
        const modalRef = this.modalService.open(InsuranceForm);
        modalRef.componentInstance.customerCar = this.car;
        let comment = this.car.car.brand.title + ' ' + this.car.car.model.title + ' ' + this.car.car.complectation.title;
        modalRef.componentInstance.comment = comment;
        modalRef.componentInstance.type = 'insurance';
    }
}
