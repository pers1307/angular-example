/**
 * Created by aperevalov on 18.10.17.
 */
import { Component, Input } from '@angular/core';
import { Insurance } from '../../../../../core/models/insurance';

@Component({
    selector: 'insurance-order-item',
    templateUrl: './insurance-order-item.html'
})
export class InsuranceOrderItem {
    @Input() order: Insurance;

    constructor() {

    }

    public checkType () {
        return this.order.type !== 'ОСАГО';
    }
}
