import { Component, OnInit } from '@angular/core';
import { CustomerCarService } from '../../../core/services/customer-car.service';
import { CustomerCar } from '../../../core/models/customer-car';
import { SellCarService } from '../../../core/services/sell-car.service';
import { CorrectPrice } from '../../../core/models/correct-price';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleForm } from '../../../shared/components/simple-form/simple-form.component';

@Component({
    selector: 'sell-car',
    templateUrl: './sell-car.component.html'
})
export class SellCarComponent implements OnInit {

    public customerCar: CustomerCar;
    public load: boolean = false;
    public error: boolean = false;
    public mockData: Array<any> = [];

    constructor(
        private customerCarService: CustomerCarService,
        private sellCarService: SellCarService,
        private modalService: NgbModal
    ) {

    }

    public ngOnInit(): void {
        this.customerCarService.customerCarCurrent$
            .subscribe((customerCar: CustomerCar) => {
                this.customerCar = customerCar;

                if (customerCar !== null) {
                    this.prepareData(this.customerCar);
                }
            });
    }

    public openForm(type: string, price: number) {
        const modalRef = this.modalService.open(SimpleForm);
        modalRef.componentInstance.customerCar = this.customerCar;
        let comment = this.customerCar.car.brand.title + ' ' + this.customerCar.car.model.title + ' ' + this.customerCar.car.complectation.title;

        if (price) {
            comment += ', ' + type + '; по цене: ' + price;
        }
        modalRef.componentInstance.comment = comment;
        modalRef.componentInstance.type = 'sell_car';
    }

    private prepareData(customerCar: CustomerCar) {
        this.load = true;

        this.sellCarService.getPrice(customerCar)
            .subscribe((response: CorrectPrice|any) => {
                this.mockData = [];

                if (
                    response !== 'noComplectation'
                    && response !== 'noExternalComplectation'
                    && response !== 'noExternalModification'
                    && response !== 'noCarYear'
                    && response !== 'noCarRun'
                    && response !== 'error'
                    && response.price !== undefined
                ) {
                    let self       = this;
                    let redemption = {};
                    let tradeIn    = {};
                    let commission = {};

                    response.price.forEach(function(priceItem, index) {
                        if (priceItem.cond === 'good' && priceItem.way === 'tradein') {
                            tradeIn = {
                                alias: 'trade-in',
                                title: 'Trade-in',
                                descr: 'Выгодный Trade-in',
                                period: '1 день',
                                price: priceItem.price
                            };
                        } else if (priceItem.cond === 'good' && priceItem.way === 'comission') {
                            redemption = {
                                alias: 'redemption',
                                title: 'Выкуп',
                                descr: 'Выкуп по рыночной стоимости',
                                period: '1 час',
                                price: priceItem.price * 0.88
                            };

                            commission = {
                                alias: 'commission',
                                title: 'Комиссия',
                                descr: 'На комиссию по вашей цене',
                                period: '> 1 месяца',
                                price: priceItem.price
                            };
                        }
                    });

                    self.mockData.push(redemption);
                    self.mockData.push(tradeIn);
                    self.mockData.push(commission);
                } else {
                    this.error    = true;
                }

                this.load = false;
            });
    }
}
