import { Component, OnInit } from '@angular/core';
import { CustomerCarService } from '../../../core/services';
import { CustomerCar, Anchor } from '../../../core/models';

import 'style-loader!./dashboard.component.scss';
import { ScrollToAnchorService } from '../../../shared/services/scroll-to-anchor/scroll-to-anchor.service';

@Component({
    selector   : 'customer-dashboard',
    templateUrl: './dashboard.component.html'
})
export class Dashboard implements OnInit {
    public car: CustomerCar;
    public load: boolean = false;
    public anchors: Array<Anchor>;

    constructor(
        private customerCarService: CustomerCarService,
        private scrolltoAnchorService: ScrollToAnchorService
    ) {
        this.anchors = [
            {
                anchor: 'current-service',
                description: 'Текущее обслуживание',
                icon: 'clock'
            },
            {
                anchor: 'car-parts-order',
                description: 'Заказ запчастей',
                icon: 'wrench'
            },
            // {
            //     anchor: 'profit',
            //     description: 'Выгодные предложения',
            //     icon: 'percent_thin'
            // }
        ];
    }

    public ngOnInit(): void {
        this.customerCarService.customerCarCurrent$
            .filter(selectedCustomerCar => selectedCustomerCar !== null)
            .subscribe(selectedCustomerCar => {
                let self = this;
                this.load = true;
                this.car = selectedCustomerCar;

                setTimeout(function() {
                    self.load = false;
                }, 1000);

                if (this.hasServiceCampaign(this.car.car) && !this.anchors.find(item => item.anchor === 'service-campaign')) {
                    let serviceCampaignAnchor: Anchor = {
                        anchor: 'service-campaign',
                        description: 'Сервисные кампании',
                        icon: 'attention_triangle_red',
                    };

                    this.anchors.splice(1, 0, serviceCampaignAnchor);
                }
            });

    }

    public hasServiceCampaign(car): boolean {
        return car.serviceCampaign && car.serviceCampaign.length > 0;
    }
}
