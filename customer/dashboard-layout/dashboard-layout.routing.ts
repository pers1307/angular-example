import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { ArrayUtils } from '../../shared/utils/array.utils';
import { AvailableModulesService } from '../../core/services/available-modules.service';
import { ConfigService } from '../../core/services/config.service';
import { CustomerGuard } from '../customer.guard';

import { DashboardLayoutComponent } from './dashboard-layout.component';
import { Dashboard } from './dashboard/dashboard.component';
import { ServiceComponent } from './service/service.component';
import { InsuranceDashboard } from './insurance-dashboard/insurance-dashboard.component';
import { SellCarComponent } from './sell-car/sell-car.component';

const menuTree = ConfigService.get('menu');
const availableModules: Array<string> = AvailableModulesService.getAvailableModules(menuTree);

let children: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: 'dashboard',
        component: Dashboard,
    }
];

if (ArrayUtils.inArray(availableModules, 'service')) {
    children = children.concat([
        {
            path: 'service',
            component: ServiceComponent,
        }
    ]);
}

if (ArrayUtils.inArray(availableModules, 'insurance-dashboard')) {
    children = children.concat([
        {
            path: 'insurance-dashboard',
            component: InsuranceDashboard,
        }
    ]);
}

if (ArrayUtils.inArray(availableModules, 'sell-car')) {
    children = children.concat([
        {
            path: 'sell-car',
            component: SellCarComponent,
        }
    ]);
}

export let routes: Routes = [
    {
        path: '',
        component: DashboardLayoutComponent,
        children: children,
        canActivate: [CustomerGuard],
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
