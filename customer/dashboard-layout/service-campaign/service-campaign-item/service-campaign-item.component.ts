/**
 * Created by asmolin on 26.05.17.
 */
import { Component, Input } from '@angular/core';
import { ServiceCampaign as ServiceCampaignModel } from '../../../../core/models';
import { ConfigService } from '../../../../core/services/config.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomerCar } from '../../../../core/models/customer-car';
import { SimpleForm } from '../../../../shared/components/simple-form/simple-form.component';

const icon = require('./icons/calendar-with-lines-in-back.svg');

@Component({
    selector   : 'service-campaign-item',
    templateUrl: './service-campaign-item.html'
})
export class ServiceCampaignItem {
    @Input() campaign: ServiceCampaignModel;
    @Input() car: CustomerCar;

    public tr5url: string;

    constructor(private config: ConfigService, private modalService: NgbModal) {
        this.tr5url = config.get('tr5Url');
    }

    public openForm(car: CustomerCar, serviceCampaign: ServiceCampaignModel) {
        const modalRef = this.modalService.open(SimpleForm);
        modalRef.componentInstance.customerCar = car;
        let comment = 'Запись на сервисную кампанию ' + serviceCampaign.title;
        if (serviceCampaign.externalId) {
            comment += ' (' + serviceCampaign.externalId + ')';
        }
        modalRef.componentInstance.comment = comment;
        modalRef.componentInstance.type = 'service_campaign';
    }
}
