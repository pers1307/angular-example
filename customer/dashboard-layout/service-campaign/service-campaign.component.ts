/**
 * Created by asmolin on 26.05.17.
 */
import { Component, OnInit } from '@angular/core';
import { ServiceCampaign as ServiceCampaignModel } from '../../../core/models';
import { CustomerCarService } from '../../../core/services/customer-car.service';
import { CustomerCar } from '../../../core/models/customer-car';


@Component({
    selector   : 'service-campaign',
    templateUrl: './service-campaign.html'
})
export class ServiceCampaign implements OnInit {
    public car: CustomerCar;
    public campaigns: Array<ServiceCampaignModel> = [];
    public load: boolean = false;

    constructor(private _customerCarService: CustomerCarService) {

    }

    public ngOnInit(): void {
        this._customerCarService.customerCarCurrent$
            .filter(currentCar => currentCar !== null)
            .subscribe(currentCar => {
                this.car = currentCar;

                if (currentCar.car.serviceCampaign) {
                    this.campaigns = currentCar.car.serviceCampaign.filter(campaign => {
                        return !campaign.dateCompletion;
                    });
                }
            });
    }
}
