import { Directive, NgZone, OnDestroy, OnInit } from '@angular/core';
import { CompressStateService } from '../services/compress-state.service';

@Directive({
    selector: '[compress]'
})
export class CompressDirective implements OnDestroy, OnInit {
    constructor(private ngZone: NgZone,
                private compressStateService: CompressStateService) {
    }

    private eventOptions: boolean;

    public compress = (): void => {
        if (pageYOffset >= 100) {
            this.ngZone.run(() => {
                this.compressStateService.setState(true);
            });
        } else {
            this.ngZone.run(() => {
                this.compressStateService.setState(false);
            });
        }
    }

    public ngOnInit(): void {
        this.eventOptions = true;

        this.ngZone.runOutsideAngular(() => {
            window.addEventListener('scroll', this.compress, this.eventOptions);
        });
    }

    public ngOnDestroy(): void {
        window.removeEventListener('scroll', this.compress, this.eventOptions);
    }
}
