import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CompressStateService {
    private subject = new Subject<boolean>();

    constructor() {

    }

    public setState(state: boolean) {
        this.subject.next(state);
    }

    public getState(): Observable<boolean> {
        return this.subject;
    }
}
