/**
 * Created by aperevalov on 07.02.18.
 */

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CustomerCarService } from '../../core/services/customer-car.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DashboardLayoutGuard implements CanActivate {
    constructor(private _customerCarService: CustomerCarService,
                private _router: Router) {
    }

    public canActivate() {
        return this._customerCarService.customerCarList$
            .flatMap((list) => {
                if (list.length > 0) {
                    return Observable.of(true);
                } else {
                    this._router.navigate(['/garage/car']);
                    return Observable.of(false);
                }
            });
    }

}
