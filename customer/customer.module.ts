import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './customer.routing';

import { Customer } from './customer.component';
import { CustomerGuard } from './customer.guard';
import { MenuBuilderService } from '../core/services/menu-builder.service';
import { DashboardLayoutGuard } from './dashboard-layout/dashboard-layout.guard';
import { CustomerScenarioGuard } from './customer-scenario.guard';

@NgModule({
    imports: [
        CommonModule,
        routing,
    ],
    declarations: [
        Customer,
    ],
    providers: [
        CustomerGuard,
        CustomerScenarioGuard,
        DashboardLayoutGuard,
        MenuBuilderService
    ]
})
export class CustomerModule {
}
