/**
 * Created by asmolin on 19.04.17.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from '../core/services/auth.service';

@Injectable()
export class CustomerGuard implements CanActivate {

    constructor(private auth: AuthService, private router: Router) {
    }

    public canActivate() {
        if (!this.auth.isLoggedIn()) {
            this.router.navigate(['auth/login']);
            return false;
        }
        return true;
    }

}