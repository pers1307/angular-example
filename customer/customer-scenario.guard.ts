import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../core/services/auth.service';
import * as _ from 'lodash';

@Injectable()
export class CustomerScenarioGuard implements CanActivate {

    constructor(private _router: Router,
                private _auth: AuthService) {
    }

    public canActivate() {
        if (this._router.url === 'profile') {
            return true;
        } else {
            return this._auth.user$
                .filter(user => !_.isEmpty(user.profile) && !_.isEmpty(user.phone))
                .map(user => {
                    if (!user.profile.name || !user.profile.lastname) {
                        this._router.navigate(['profile']);
                        return false;
                    } else {
                        return true;
                    }
                });
        }
    }

}

